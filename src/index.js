import React from 'react';
import ReactDOM from 'react-dom';
import Home from './components/Home';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import './assets/css/style.css'
import './assets/css/media.css'

ReactDOM.render(
	<Router>
        <div className="div">
	        <Route exact path="/" component={Home}/>
       	</div>
    </Router>,
	document.getElementById('root'));
registerServiceWorker();
