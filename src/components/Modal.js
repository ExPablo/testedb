import React from 'react';
import Modal from 'react-modal'
import '../assets/css/modalstyle.css'


const customStyles = {
  content : {
    top                   : '10%',
    left                  : '25%',
    right                 : '85%',
    bottom                : '60%',
    marginRight           : '-60%',
    border                : 'solid #ff6c06 5px',
    textAlign             : 'center',
    // transform             : 'translate(-50%, -50%)',
    zIndex                : '999'
  }
};

// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
class Rmodal extends React.Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false,
    };

    this.closeModal = this.closeModal.bind(this);
  }
  componentWillMount(){
    if(this.props.isOpen > 0){
      this.setState({modalIsOpen: true})
    }
  }
  

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  render() {
    return (
      <div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          contentLabel="Detalhes"
          style={customStyles}
        >
        <div className="limiter">
          <h1 style={{color:'#ff6c06', fontSize: '50px'}}>Caution</h1>
          <span>you have {this.props.isOpen} beers outside of the temperature range.</span>
          <br/>
          <button style={{background: '#ff6c06', border: 'none', padding: '5px 10px', color: '#fff', marginTop: '10px'}} onClick={this.closeModal}>Ok</button>
        </div>
        </Modal>
      </div>
    );
  }
}

export default Rmodal