import React from 'react';

class Header extends React.Component{
	state = {
	}
	componentWillMount = ()=>{
	}
	componentDidMount = ()=>{
	}
    render(){
        return(
			<header>
				<a href="/" className="logo">
					<img src="./assets/img/LogoDB.png" alt="logo"/>
				</a>
				<ul className="menu">
					<li onClick={() => this.props.option('show')}>Show Status</li>
					<li onClick={() => this.props.option('change')}>Change temperature</li>
				</ul>
			</header>
        );

    }
}

export default Header;