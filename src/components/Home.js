import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Modal from './Modal';
class Home extends React.Component{
	state = {
		warning: 0,
		status:true,
		change:false,
		beer: [
			{
				id:1,
				name: 'Beer (Pilsner)',
				status: 'good',
				temp: -5,
				min: -4,
				max: -6
			},
			{
				id:2,
				name: 'Beer (IPA)',
				status: 'good',
				temp: -6,
				min: -5,
				max: -6
			},
			{
				id:3,
				name: 'Beer (Lager)',
				status: 'good',
				temp: -5,
				min: -4,
				max: -7
			},
			{
				id:4,
				name: 'Beer (Stout)',
				status: 'good',
				temp: -7,
				min: -6,
				max: -8
			},
			{
				id:5,
				name: 'Beer (Wheat beer)',
				status: 'good',
				temp: -4,
				min: -3,
				max: -5
			},
			{
				id:6,
				name: 'Beer (Pale Ale)',
				status: 'warning',
				temp: -7,
				min: -4,
				max: -6
			},


		]
	}
	componentWillMount = () => {
	}
	componentDidMount = () => {
	}
	option (x) {
		this.changeOption(x)
	}
	changeOption (x) {
		if(x === 'show'){
			this.setState({change: false, warning: 0})
			this.changeStatus()
		}else{
			this.setState({change: true, warning: 0})
		}
	}
	changeStatus () {
		const beer = this.state.beer
		var cont = 0
		beer.map(n => {
			if(n.temp > n.min || n.temp < n.max){
				cont = cont + 1
				n.status = 'warning'
			}else{
				n.status = 'good'
			}

		})
		this.setState({warning:cont})
	}
	changeTemp (e, id) {
		const val = e.target.value
		const beer = this.state.beer
		beer.map(n => {
			if(n.id === id){
				n.temp = val
			}
		})
		this.setState({beer:beer})
	}
    render(){
    	const {
    		warning,
    		status,
    		change,
    		beer
    	} = this.state
        return(
          	<div>
          		<Header option={(x) => this.option(x)}/>
          		{
          			warning > 0? <Modal isOpen={warning}/> : ''
          		}
				<main>
					<div className="limiter">
						<div className={change?'change':'none'}>
							<ul>
								{
									beer.map((v,k) => {
										return 	<li key={k}>
													<div className="changeBox">
														<ul>
															<li>{v.name}</li>
															<li>Ideal: {v.min}ºc {v.max}ºc</li>
															<li>
																Temperature:
																<input type="number" value={v.temp} onChange={(e) => this.changeTemp(e , v.id)}/>
															</li>
														</ul>
													</div>
												</li>
									})
								}
							</ul>
						</div>
	          			<div className={status?'show':'none'}>
		          			<ul className="itens-list">
		          			{
		          				beer.map((n, index) => {
		          					return  <li className={n.status} key={index}>
		          						   		<div className="itens">
		          									<img src={`./assets/img/${n.id}.jpg`} alt=''/>
		          									<div className="description">
		          										<ul>
		          											<li>{n.name}</li>
		          											<li>
				 												<span className={n.status === 'good'? 'status good': 'status warning'}>{n.status}</span>
		          											</li>
		          											<li className={n.status}>{n.temp}ºc</li>
		          										</ul>
		          									</div>
		          								</div>
		          							</li>
		          				})
		          			}
		          				
		          			</ul>
	          			</div>
					</div>
		        </main>
		        <Footer/>
			</div>
        );

    }
}

export default Home;